/*
 * Copyright (c) 2020-2020, org.smartboot. All rights reserved.
 * project name: smart-license
 * file name: LicenseProtocol.java
 * Date: 2020-03-26
 * Author: sandao (zhengjunweimail@163.com)
 */

package org.smartboot.license.client;

import java.io.Serializable;

/**
 * @author 三刀
 * @version V1.0 , 2020/3/26
 */
public final class LicenseEntity implements Serializable {

    /**
     * 魔数
     */
    public static final byte[] MAGIC_NUM = "smart-license".getBytes();
    /**
     * 公钥
     */
    private final byte[] publicKeys;
    /**
     * 申请时间
     */
    private final long applyTime = System.currentTimeMillis();

    /**
     * 过期时间
     */
    private final long expireTime;

    /**
     * 申请方
     */
    private String applicant;

    /**
     * 联系方式
     */
    private String contact;

    /**
     * 试用时长
     */
    private int trialDuration;

    /**
     * 原文
     */
    private transient byte[] data;

    public LicenseEntity(long expireTime, byte[] publicKeys) {
        this.expireTime = expireTime;
        this.publicKeys = publicKeys;
    }

    public long getExpireTime() {
        return expireTime;
    }

    public byte[] getPublicKeys() {
        return publicKeys;
    }

    public long getApplyTime() {
        return applyTime;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public int getTrialDuration() {
        return trialDuration;
    }

    public void setTrialDuration(int trialDuration) {
        this.trialDuration = trialDuration;
    }
}
